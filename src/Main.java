import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
//        System.out.printf("Hello and welcome!");
//
//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }
//
//        Урок 6. Ветвление
//        Задания:
//        1.
//        Пользователь вводит дробное число. Если оно больше пи, вывести
//“pimore”

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите num");
        double num = scanner.nextDouble();
        if (num>Math.PI){
            System.out.println("pimore");
        } else {
            System.out.println("it's ok");
        }
//        2.
//        Пользователь вводит строку. Используя метод .contains(‘ ‘)
//        пробел, определите, ввел пользователь одно слово, или больше


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("введите строку из одного или нескольких слов");
        String str;
                str=scanner.nextLine();

        str=scanner.nextLine();
        if (str.contains(" ")){
            System.out.println("в строке есть пробелы, видимо вы ввели несколько слов");
        } else {
            System.out.println("похоже что вы ввели только одно слово");
        }

//        3.
//        Пользователь вводит четыре числа. Найти наибольшее из них.

        System.out.println("введите 4 числа");
        num = scanner.nextDouble();
        int num2 = scanner.nextInt();
        int num3 = scanner.nextInt();
        int num4 = scanner.nextInt();
        System.out.print("вот это число наибольшее ");
        System.out.println(Math.max(Math.max(num,num2),Math.max(num3,num4)));


//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для ввода данных со стороны пользователя
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                за
//        мечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов



    }
}